import logging
import pandas as pd
from simpletransformers.t5 import T5Model, T5Args
import itertools

import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')

logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)


model_args = T5Args()
model_args.num_train_epochs = 200
model_args.no_save = True
model_args.train_batch_size = 32
model_args.learning_rate = 1e-5
model_args.eval_batch_size = 640

model = T5Model("t5", "t5-base", args=model_args)

types = ['intent']  # ['slot', 'intent']
# ['snips', 'atis', 'multiwoz', 'sgd']
datasets = ['sgd']
for execute_type in types:
    for dataset in datasets:
        all_data = list()
        sentences = list()
        with open('{}_{}_mixed_compare.txt'.format(dataset, execute_type), 'r') as reader:
            for line in reader:
                splitted_line = line.strip().split('\t')
                all_data.append(splitted_line)
                sentences.append(splitted_line[-1])
        predictions = model.predict(sentences)

        new_all_data = list()
        for item, prediction in zip(all_data, predictions):
            new_all_data.append('\t'.join(item + [prediction]))
        with open('{}_{}_mixed_compare_result.txt'.format(dataset, execute_type), 'w') as writer:
            for item in new_all_data:
                writer.write(item + '\n')
        
        all_data.clear()
        sentences.clear()
        with open('{}_{}_mixed_desc_compare.txt'.format(dataset, execute_type), 'r') as reader:
            for line in reader:
                splitted_line = line.strip().split('\t')
                all_data.append(splitted_line)
                sentences.append(splitted_line[-1])
        predictions = model.predict(sentences)

        new_all_data.clear()
        for item, prediction in zip(all_data, predictions):
            new_all_data.append('\t'.join(item + [prediction]))
        with open('{}_{}_mixed_desc_compare_result.txt'.format(dataset, execute_type), 'w') as writer:
            for item in new_all_data:
                writer.write(item + '\n')
