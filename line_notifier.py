# -*- coding: UTF-8 -*-

import argparse
import datetime
import os
import time

from linebot import LineBotApi
from linebot.models import TextSendMessage

ACCESS_TOKEN = 'BbmKpQMrtGv1XW1VjELUmTrAiN0LtRP++G3wDjydZWmNqT6LNIFBrpVnApZIfflgmni8UO3khkFO0CXG+aiSEUcnriuJJlf90IShS7y7SzQ6TzIirWG8BSX7vvxawUiFArvZOYOzAEUN7OeBFanpPQdB04t89/1O/w1cDnyilFU='
# USER_OR_GROUP_ID = 'Ce7808727837e7f78976c52ed96039b70'
USER_OR_GROUP_ID = 'Ue4721fa9638cddd6d20101715f77f3f1'


def getModelMessage(doer, jobName, operation):
    hostname = os.uname()[1]
    nowTS = time.time()
    now = datetime.datetime.fromtimestamp(nowTS).strftime('%Y-%m-%d %H:%M %p')
    form = '*******Job Notification*******\nDoer: {}\nJob Name: {}\nJob Item: {}\nhost: {}\nTime: {}'

    return form.format(doer, jobName, operation, hostname, now)


def sendMessage(doer, jobName, operation):
    line_bot_api = LineBotApi(ACCESS_TOKEN)
    line_bot_api.push_message(USER_OR_GROUP_ID,
                              TextSendMessage(text=getModelMessage(doer, jobName, operation)))

# if __name__ == "__main__":
#     parser = argparse.ArgumentParser()
#     parser.add_argument('--doer')
#     parser.add_argument('--jobName')
#     parser.add_argument('--operation')

#     args = parser.parse_args()

#     line_bot_api = LineBotApi(ACCESS_TOKEN)
#     line_bot_api.push_message(USER_ID,
#                               TextSendMessage(text=getModelMessage(args.doer, args.jobName, args.operation)))
