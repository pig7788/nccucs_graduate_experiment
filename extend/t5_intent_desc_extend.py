import logging
import pandas as pd
from simpletransformers.t5 import T5Model, T5Args
import itertools

import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')

logging.basicConfig(level=logging.INFO)
transformers_logger = logging.getLogger("transformers")
transformers_logger.setLevel(logging.WARNING)


train_df = pd.read_excel(
    'sgd_intent_desc_train.xlsx', engine='openpyxl')
# slot_filling_test_df = pd.read_excel(
#     'sgd_slot_filling_test.xlsx', engine='openpyxl')
intent_test_df = pd.read_excel(
    'sgd_intent_desc_test.xlsx', engine='openpyxl')
# slot_filling_test_sent = list(itertools.chain.from_iterable(
#     slot_filling_test_df.values))
intent_test_sent = list(itertools.chain.from_iterable(
    intent_test_df.values))

model_args = T5Args()
model_args.num_train_epochs = 200
model_args.no_save = True
model_args.train_batch_size = 16
model_args.learning_rate = 1e-5
model_args.eval_batch_size = 560

model = T5Model("t5", "t5-base", args=model_args)
model.train_model(train_df)

# predictions = model.predict(slot_filling_test_sent)
# with open('sgd_slot_filling_desc_pred.txt', 'w') as writer:
#     for prediction in predictions:
#         writer.write(prediction + '\n')

predictions = model.predict(intent_test_sent)
with open('sgd_intent_desc_pred.txt', 'w') as writer:
    for prediction in predictions:
        writer.write(prediction + '\n')
